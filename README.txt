CONTENTS OF THIS FILE
---------------------
 * Introduction
    Bulk File Upload Module provides user interface to upload multiple files 
    in with a zip format & extracts it to any folder within public folder.

 * Requirements
    It will help Users to upload multiple files at the same time in a selected
    folder or new folder.
    
 * Installation
    Follow this URL : 
    https://drupal.org/documentation/install/modules-themes/modules-7
    
 * Maintainers
    Mehul Shah : https://drupal.org/user/2220074
    Nitesh Pawar : https://drupal.org/user/1069334
