<?php

/**
 * @file
 * Administration forms for bulk file upload.
 */

/**
 * Form used to collect data.
 */
function bulk_file_upload_form($form, &$form_state) {
  $temp = bulk_file_upload_get_directory();
  $form['path'] = array(
    '#type' => 'select',
    '#title' => t('Select File Upload Location'),
    '#options' => array('new' => 'New Folder') + $temp,
    '#required' => TRUE,
    '#ajax' => array(
      'callback' => 'bulk_file_upload_get_textfield',
      'wrapper' => 'directory_textfield',
    ),
  );
  $form['directory_name'] = array(
    '#prefix' => '<div id="directory_textfield">',
    '#suffix' => '</div>',
  );
  if (isset($form_state['values']['path']) && $form_state['values']['path'] == 'new') {
    $form['directory_name'] = array(
      '#title' => t("Folder Name"),
      '#description' => t('Enter Folder Name'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#prefix' => '<div id="directory_textfield">',
      '#suffix' => '</div>',
      '#size' => 30,
    );
  }
  $form['file_extension'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed File Extension'),
    '#description' => t('Use comma to separate multiple extensstion<br/> Ex. jpg,jpeg'),
    '#size' => 96,
    '#required' => TRUE,
  );
  $max_upload_size = ini_get("upload_max_filesize");
  $form['zip_file'] = array(
    '#type' => 'file',
    '#title' => t('Upload Zip File'),
    '#description' => t('Warning : MAX SIZE LIMIT : @max_upload_size<br/>ZIP File should contain Files directly, not in Folder', array('@max_upload_size' => $max_upload_size)),
    '#size' => 48,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Upload',
  );
  return $form;
}

/**
 * Ajax Callback.
 */
function bulk_file_upload_get_textfield($form, &$form_state) {
  return $form['directory_name'];
}

/**
 * Validate function for settings form.
 */
function bulk_file_upload_form_validate($form, &$form_state) {
  $error = 0;
  $setting_extension = explode(',', str_replace(' ', '', $form_state['values']['file_extension']));
  $selected_path = $form_state['values']['path'];
  $directory = 'public://';
  if ($selected_path == 'new') {
    if (isset($form_state['values']['directory_name'])) {
      $new_dir = $form_state['values']['directory_name'];
      if (in_array($new_dir, bulk_file_upload_get_directory())) {
        form_set_error('directory_name', t("Folder with given name is already present"));
        $error = 1;
      }
      elseif (is_writable($directory)) {
        mkdir($directory . '/' . $new_dir, 0755);
        $selected_path = $new_dir;
      }
      else {
        form_set_error('path', t("You do not have enough permissions to create a new folder"));
      }
    }
  }

  $directory .= '/' . $selected_path;
  if (!$error) {
    if (is_writable($directory) || $selected_path == 'new') {
      $file = file_save_upload('zip_file', array('file_validate_extensions' => array('zip')), 'public://' . $selected_path, FILE_EXISTS_REPLACE);
      if (isset($file) && !empty($file)) {
        $zip = new ZipArchive();
        $zip = zip_open(drupal_realpath($file->uri));
        $count = 0;
        $invalid_files = '';
        if (is_resource($zip)) {
          while ($zip_entry = zip_read($zip)) {
            $file_name = zip_entry_name($zip_entry);
            $extension = explode(".", $file_name);
            $extension = end($extension);
            if (!in_array($extension, $setting_extension)) {
              $count++;
              $invalid_files[] = $count . ')' . $file_name;
            }
          }
          $zip = zip_close($zip);
          if ($count > 0) {
            form_set_error('zip_file', t("An error occurred and processing did not complete.<br/>Please Upload only files with mentioned extension.<br/>There are @count invalid files as<br/>", array('@count' => $count)));
            foreach ($invalid_files as $invalid_file) {
              drupal_set_message(t('@file_name', array('@file_name' => $invalid_file)), 'error');
            }
            unlink(drupal_realpath($file->uri));
          }
          else {
            $form_state['values']['path'] = $selected_path;
            $form_state['values']['file'] = $file;
          }
        }
      }
      else {
        form_set_error('zip_file', t("Please upload a zip file."));
      }
    }
    else {
      form_set_error('path', t("The selected Directory @directory is not writable", array('@directory' => $selected_path)));
    }
  }
}

/**
 * Validate function for settings form.
 */
function bulk_file_upload_form_submit($form, &$form_state) {
  $selected_path = $form_state['values']['path'];
  $path_check = file_create_url('public://') . $selected_path . '/';
  $file = $form_state['values']['file'];
  $zip = new ZipArchive();
  if ($zip->open(drupal_realpath($file->uri)) === TRUE) {
    $zip->extractTo('public://' . $selected_path);
    $zip->close();
    drupal_set_message(t("File Uploaded Successfully<br/> You can check uploaded file using @path_check+File name", array('@path_check' => $path_check)), 'status');
    unlink(drupal_realpath($file->uri));
  }
}

/**
 * Implements helper function.
 */
function bulk_file_upload_get_directory() {
  $folders = array();
  if ($handle = opendir('public://')) {
    while (FALSE !== ($entry = readdir($handle))) {
      if ($entry != "." && $entry != ".." && is_dir('public://' . $entry)) {
        $folders[$entry] = $entry;
      }
    }
    closedir($handle);
  }
  return $folders;
}
